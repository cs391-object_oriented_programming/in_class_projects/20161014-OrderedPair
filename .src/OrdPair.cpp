#include<iostream>

using namespace std;

class OrdPair{
public:
  OrdPair( ){   //default constructor to initialize values
    p1 = 0.0;
    p2 = 0.0;
  }
  OrdPair( float f1 , float f2 ) {  //constructor
    p1 = f1;
    p2 = f2;
  }
  bool operator==(const OrdPair& ) const;  //prototypes
  OrdPair operator*( const OrdPair& ) const;
  OrdPair operator-(const OrdPair& ) const;
  void write_it( ) const;
private:
  float p1, p2;
};

bool OrdPair::operator==(const OrdPair& s) const { 
  return p1 == s.p1 && p2 == s.p2;
}

OrdPair OrdPair::operator*( const OrdPair& s) const { //use const to avoid altering member values
  OrdPair z(p1*s.p1, p2*s.p2);
  return z;
 }

OrdPair OrdPair::operator-( const OrdPair& s) const {
  OrdPair z(p1 - s.p1, p2 - s.p2);
  return z;
}

void OrdPair::write_it( ) const {
  cout << "The result is: (" << p1 << "," << p2 << ")" << endl; 
}

int main()
{
  OrdPair s1(32, 2), s2(32,-2), s3;
  if(s1.operator==(s2))
   // or can use 
    //if (s1 == s2) 
    cout << "equal" << endl;
  else 
    cout<< "Not equal" << endl;
   
 // s3 = s1.operator*(s2);      
 s3 = s1 * s2;
  
  // s3 = s1.operator-(s2);
 s3 = s1 - s2; 
 s3.write_it( ); 
 
  return 0;
}
